(function () {
    let Bluebird = require('bluebird');
    let fs = Bluebird.promisifyAll(require("fs"));

    module.exports = {

        read: function (fileName) {
            return fs.readFileAsync(fileName, "utf8");
        }
    };

})();
