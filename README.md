# redige-html-parser

An HTML parser used by Redige to the paste feature & markdown conversion.

## Usage

~~~javascript
function parse(htmlString){
    let parser = new HTMLParser();
    return parser.parse(htmlString);
}
~~~

## options

~~~javascript
function parse(htmlString){
    const options = {
        saxHandler : new HTMLParser.DefaultHTMLSaxHandler(), /*<= this is default value. You can extends it.*/
        nomalize   : new HTMLParser.DefaultHTMLNormalize()     /*<= this is default value. You can extends it.*/
    }
    let parser = new HTMLParser();
    return parser.parse(htmlString,options);
}
~~~